//  copyright lexilogos.com
var car;
function transcrire() {
car = document.conversion.saisie.value;
    // harakat;
car = car.replace(/F/g, "ً");
car = car.replace(/K/g, "ٍ");
car = car.replace(/N/g, "ٌ");
car = car.replace(/a/g, "َ");
car = car.replace(/i/g, "ِ");
car = car.replace(/u/g, "ُ");
car = car.replace(/~/g, "ّ");
car = car.replace(/o/g, "ْ");

    // letters
car = car.replace(/`/g, "ٰ");
car = car.replace(/A/g, "ا");
car = car.replace(/\{/g, "ٱ");
car = car.replace(/\|/g, "آ");
car = car.replace(/b/g, "ب");
car = car.replace(/ب'/g, "پ");
car = car.replace(/t/g, "ت");
car = car.replace(/v/g, "ث");
car = car.replace(/j/g, "ج");
car = car.replace(/ج'/g, "چ");
car = car.replace(/H/g, "ح");
// car = car.replace(/’/g, "\'");
// car = car.replace(/[AÂÀĀ]/g, "ا");
// car = car.replace(/p/g, "پ");
// car = car.replace(/پ'/g, "ب");
// car = car.replace(/ث'/g, "ت");
// car = car.replace(/ṯ/g, "ث");
// car = car.replace(/c/g, "چ");
// car = car.replace(/چ'/g, "ح");
// car = car.replace(/ح'/g, "خ");
// car = car.replace(/خ'/g, "ج");
// car = car.replace(/ذ'/g, "د");
// car = car.replace(/ḏ/g, "ذ");
// car = car.replace(/ر'/g, "ز");
// car = car.replace(/ز'/g, "ر");
// car = car.replace(/س'/g, "ش");
// car = car.replace(/ش'/g, "س");
// car = car.replace(/š/g, "ش");
// car = car.replace(/ص'/g, "ض");
// car = car.replace(/ض'/g, "ص");
// car = car.replace(/ط'/g, "ظ");
// car = car.replace(/ظ'/g, "ط");
// car = car.replace(/ع'/g, "غ");
// car = car.replace(/غ'/g, "ع");
// car = car.replace(/ġ/g, "غ");
// car = car.replace(/ڤ'/g, "ف");
// car = car.replace(/v/g, "ڢ");
// car = car.replace(/ڨ'/g, "ق");
// car = car.replace(/ڭ'/g, "ك");
// car = car.replace(/ه'/g, "ة");
// car = car.replace(/ة'/g, "ه");
// car = car.replace(/[wouôûōū]/g, "و");
// car = car.replace(/و'/g, "ؤ");
// car = car.replace(/ؤ'/g, "و");
// car = car.replace(/ي'/g, "ى");
// car = car.replace(/ى'/g, "ئ");
// car = car.replace(/ئ'/g, "ي");
// car = car.replace(/ʾ/g, "ء");
// car = car.replace(/وءء/g, "ؤ");
// car = car.replace(/يءء/g, "ئ");
// car = car.replace(/I/g, "إ");
// car = car.replace(/A/g, "إ");
car = car.replace(/x/g, "خ");
car = car.replace(/d/g, "د");
car = car.replace(/\*/g, "ذ");
car = car.replace(/r/g, "ر");
car = car.replace(/z/g, "ز");
car = car.replace(/s/g, "س");
car = car.replace(/\$/g, "ش");
car = car.replace(/S/g, "ص");
car = car.replace(/D/g, "ض");
car = car.replace(/T/g, "ط");
car = car.replace(/Z/g, "ظ");
car = car.replace(/E/g, "ع");
car = car.replace(/g/g, "غ");
car = car.replace(/f/g, "ف");
car = car.replace(/ف'/g, "ڤ");
car = car.replace(/q/g, "ق");
car = car.replace(/ق'/g, "ڨ");
car = car.replace(/k/g, "ك");
car = car.replace(/ك'/g, "ڭ");
car = car.replace(/l/g, "ل");
car = car.replace(/m/g, "م");
car = car.replace(/n/g, "ن");
car = car.replace(/h/g, "ه");
car = car.replace(/p/g, "ة");
car = car.replace(/w/g, "و");
car = car.replace(/\&/g, "ؤ");
car = car.replace(/y/g, "ي");
car = car.replace(/Y/g, "ى");
car = car.replace(/'/g, "ء");
car = car.replace(/\}/g, "ئ");
car = car.replace(/\</g, "إ");
car = car.replace(/\>/g, "أ");

    // punctuation
car = car.replace(/_/g, "ـ");   //tatwi___il
car = car.replace(/\?/g, "؟");
car = car.replace(/\;/g, "؛");
car = car.replace(/\,/g, "،");

    // numbers
car = car.replace(/0=/g, "٠");
car = car.replace(/1=/g, "١");
car = car.replace(/2=/g, "٢");
car = car.replace(/3=/g, "٣");
car = car.replace(/4=/g, "٤");
car = car.replace(/5=/g, "٥");
car = car.replace(/6=/g, "٦");
car = car.replace(/7=/g, "٧");
car = car.replace(/8=/g, "٨");
car = car.replace(/9=/g, "٩");
car = car.replace(/%=/g, "٪");  //percentage
car = car.replace(/﵌/g, "﵌");

// not working yet ====v
// car = car.replace(/\;=/g, "٬");  //thousands separator
// car = car.replace(/\,=/g, "٫");  //decimal separator

startPos = document.conversion.saisie.selectionStart;
endPos = document.conversion.saisie.selectionEnd;

beforeLen = document.conversion.saisie.value.length;
afterLen = car.length;
adjustment = afterLen - beforeLen;

document.conversion.saisie.value = car;

document.conversion.saisie.selectionStart = startPos + adjustment;
document.conversion.saisie.selectionEnd = endPos + adjustment;

var obj = document.conversion.saisie;
obj.focus();
obj.scrollTop = obj.scrollHeight;
}
